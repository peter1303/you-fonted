const API = {
	"prod": "https://youapi.pdev.top/",
	"dev": "http://you.pdev.top/",
	"local": "http://localhost:8080/",
	"mock": "http://127.0.0.1:4523/m1/1576205-0-default"
}
const BASE_URL = API['prod']
	
const install = (Vue, vm) => {
	Vue.prototype.$u.http.setConfig({
		baseUrl: BASE_URL,
		loadingText: '努力加载中~',
		loadingTime: 10000,
		// ......
	});

	// 请求拦截部分，每次请求前都会执行
	Vue.prototype.$u.http.interceptor.request = (config) => {
		const token = uni.getStorageSync('token');
		config.header.token = token;
		
		return config;
		// 如果return一个false值，则会取消本次请求
		// if(config.url == '/user/rest') return false; // 取消某次请求
	}
	// 响应拦截，如配置，每次请求结束都会执行本方法
	Vue.prototype.$u.http.interceptor.response = (response) => {
		if (response.code == 200) {
			// res为服务端返回值，可能有code，result等字段
			// 这里对res.result进行返回，将会在this.$u.post(url).then(res => {})的then回调中的res的到
			// 如果配置了originalData为true，请留意这里的返回值
			return response;
		} else if (response.code == 300) {
			vm.$u.toast("请先注册账号");	
			//这里跳转登录
			setTimeout(() => {
				vm.$u.route({
					type: 'reLaunch',
					url: '/pages/register/register'
				})
			}, 1000)
			return false;
		} else {
			// 如果返回false，则会调用Promise的reject回调，
			// 并将进入this.$u.post(url).then().catch(res=>{})的catch回调中，res为服务端的返回值
			vm.$u.toast(response.msg);			
			return false;
		}
	}
}

export default {
	install
}
