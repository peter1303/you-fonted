import Vue from 'vue'
export default{
	//获取社团列表
	 getAssociations : (params = {}) => Vue.prototype.$u.get('/association'),
	 //请求加入社团
	 joinAssociations : (params = {}) => Vue.prototype.$u.post('/association/join/request',params),
	 //获取请求加入的学生列表
	 auditJoinList : (params = {}) => Vue.prototype.$u.get('/association/audit'),
	 //通过加入社团
	 passJoin : (params = {}) => Vue.prototype.$u.put('/association/pass',params),
	 //拒绝加入
	 rejectJoin : (params = {}) => Vue.prototype.$u.put('/association/reject',params),
	 
	 //添加社团
	 addClubs : (params = {}) => Vue.prototype.$u.post('/association',params),
	 //删除社团
	 delClub : (params = {}) => Vue.prototype.$u.delete('/association',params),
	 //修改社团名称
	 changeClubsName : (params = {}) => Vue.prototype.$u.put('/association/name',params),
	 //修改社团简介
	 changeClubsSummary : (params = {}) => Vue.prototype.$u.put('/association/summary',params),
	 
	 //获取社员列表
	 getMembers : (params = {}) => Vue.prototype.$u.get('/member',params),
	 //剔除社员
	 delMember : (params = {}) => Vue.prototype.$u.delete('/member',params),
	 
	 //指派负责老师
	 assignTea : (params = {}) => Vue.prototype.$u.post('/association/admin',params),
	 //销权负责老师
	 revokeTea : (params = {}) => Vue.prototype.$u.delete('/association/admin',params),
	 
	 //指派负责人
	 assignStu : (params = {}) => Vue.prototype.$u.post('/association/manager',params),
	 //销权负责人
	 revokeStus : (params = {}) => Vue.prototype.$u.delete('/association/manager',params),
}