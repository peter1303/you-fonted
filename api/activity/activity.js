import Vue from 'vue'
export default{
	//发布社团
	actPublish: (params = {}) => Vue.prototype.$u.post('/activity',params),
	//获取列表
	actLists: (params = {}) => Vue.prototype.$u.get('/activity/list',params),
	//报名
	actPar: (params = {}) => Vue.prototype.$u.post('/activity/participate',params),
	//删除活动
	delActivity: (params = {}) => Vue.prototype.$u.delete('/activity',params),
}