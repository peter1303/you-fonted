import Vue from 'vue'
export default{
	//获取用户信息
	getMess : (params = {}) => Vue.prototype.$u.get('/user/info'),
	//获取用户资料
	getEditMess : (params = {}) => Vue.prototype.$u.get('/user/profile'),	
	//保存用户资料
	saveMess : (params = {}) => Vue.prototype.$u.put('/user/profile/set',params),	
	//注销账号
	logoffUser : (params = {}) => Vue.prototype.$u.delete('/user/account'),
	
	//获取所有用户
	getUsers : (params = {}) => Vue.prototype.$u.get('/user'),
	
	//权限转移
	transferUser : (params = {}) => Vue.prototype.$u.put('/user/transfer'),
}