import Vue from 'vue'
export default{
	//发布普通帖子
	posts : (params = {}) => Vue.prototype.$u.post('/post/post',params),
	//删除
	delCard : (params = {}) => Vue.prototype.$u.delete('/post',params),
	//获取帖子
	getAcard : (params = {}) => Vue.prototype.$u.get('/post',params),
	//获取列表
	getCards : (params = {}) => Vue.prototype.$u.get('/post/list',params),
	//获取我的帖子
	getUserCards : (params = {}) => Vue.prototype.$u.get('/post/list/user',params),
	
	//获取帖子评论
	getCardCom : (params = {}) => Vue.prototype.$u.get('/comment/post',params),
	//删除评论
	delComment : (params = {}) => Vue.prototype.$u.delete('/comment',params),
	
	//发评论
	makeComment : (params = {}) => Vue.prototype.$u.post('/comment',params),
	//点赞
	like : (params = {}) => Vue.prototype.$u.post('/like',params),
	likeCancel : (params = {}) => Vue.prototype.$u.delete('/like',params),
}