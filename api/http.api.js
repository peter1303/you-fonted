import clubs from "@/api/clubs/clubs.js"
import users from "@/api/users/users.js"
import posts from "@/api/posts/posts.js"
import activity from "@/api/activity/activity.js"
import ai from "@/api/ai/ai.js"
// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等操作，更多内容详见uView对拦截器的介绍部分：
const install = (Vue, vm) => {
	//登陆
	let getLogin = (params = {}) => vm.$u.put('/user/login',params);

	//老师注册
	let regisTeacher = (params = {}) => vm.$u.post('/user/register/teacher', params);
	//学生注册
	let regiStudent = (params = {}) => vm.$u.post('/user/register/student', params);
	
	//获取班级
	let getClass = (params = {}) => vm.$u.get('/get/class',params)
	
	//获取校区
	let getCampus = (params = {}) => vm.$u.get('/get/campus',params)
	
	//获取学院
	let getInstitute = (params = {}) => vm.$u.get('/get/institute',params)
	
	//删除校区
	let deleteCampus = (params = {}) => vm.$u.delete('/campus',params)
	//删除学院
	let deleteInstitute = (params = {}) => vm.$u.delete('/institute',params)
	//删除班级
	let deleteClass = (params = {}) => vm.$u.delete('/class',params)
	
	//添加校区
	let addCampus = (params = {}) => vm.$u.post('/campus',params)
	//添加学院
	let addInstitute = (params = {}) => vm.$u.post('/institute',params)
	//添加班级
	let addClass = (params = {}) => vm.$u.post('/class',params)
	
	
	
	
	// 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
	vm.$u.api = {
		...clubs,
		...users,
		...posts,
		...activity,
		...ai,
		getLogin,
		regisTeacher,
		regiStudent,
		getClass,
		getCampus,
		getInstitute,
		deleteCampus,
		deleteInstitute,
		deleteClass,
		addCampus,
		addInstitute,
		addClass,
	};
}

export default {
	install
}